#coding=utf-8
import scrapy
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor
import requests
import os
from oscar.apps.catalogue.models import Product, ProductClass, ProductImage, Category, ProductCategory, AttributeOptionGroup, AttributeOption, ProductAttribute
from oscar.apps.partner.models import Partner, StockRecord
from oscar.apps.catalogue.categories import create_from_breadcrumbs
from zapas.settings import SCRAPY_DRESS_URL_FILE, MEDIA_ROOT

class ProductItem(scrapy.Item):
    url = scrapy.Field()


class Fason(CrawlSpider):
    name='fason'
    allowed_domains = ["fason-m.com.ua"]
    start_urls = ['http://fason-m.com.ua/catalog/dresses']

    rules = (
        Rule(LinkExtractor(restrict_xpaths=['//*[@class="col-md-4 product product-item"]']),'parse_post'),
        Rule(LinkExtractor(restrict_xpaths=['//*[@class="pagination"]/li']), follow=True),
    )

    def parse_post(self,response):
        prod = ProductItem()
        prod['url'] = response.url
        print(prod['url'])
        return prod


class Gepur(CrawlSpider):
    name='gepur'
    allowed_domains = ['gepur.com']
    start_urls = [
        'http://gepur.com/catalog/vechernie-platya',
        'http://gepur.com/catalog/sarafan',
        'http://gepur.com/catalog/platya-mini',
        'http://gepur.com/catalog/platya-midi',
        'http://gepur.com/catalog/platya-maksi',
        'http://gepur.com/catalog/teplye-platya',
        'http://gepur.com/catalog/tuniki',
        'http://gepur.com/catalog/sportivnye-platya',
        'http://gepur.com/catalog/platya-bolshih-razmerov',
        'http://gepur.com/catalog/dlya-budushih-mam'
    ]

    rules = (
        Rule(LinkExtractor(restrict_xpaths=['//*[@class="main-item"]']),'parse_post'),
        Rule(LinkExtractor(restrict_xpaths=['//*[@class="pagination"]/li']), follow=True),
    )

    cat_by_urls={
    b'http://gepur.com/catalog/vechernie-platya':'Вечерние',
    b'http://gepur.com/catalog/sarafan':'Сарафаны',
    b'http://gepur.com/catalog/platya-mini':'Мини',
    b'http://gepur.com/catalog/platya-midi':'Миди',
    b'http://gepur.com/catalog/platya-maksi':'Макси',
    b'http://gepur.com/catalog/teplye-platya':'Теплые',
    b'http://gepur.com/catalog/tuniki':'Туники',
    b'http://gepur.com/catalog/sportivnye-platya':'Спортивные',
    b'http://gepur.com/catalog/platya-bolshih-razmerov':'Большие размеры',
    b'http://gepur.com/catalog/dlya-budushih-mam':'Для будущих мам'
    }
    product_class=ProductClass.objects.all()[0]
    partner = Partner.objects.all().first()
    try:
        attrgroup = AttributeOptionGroup.objects.get(name=u'Цвет')
    except:
        attrgroup = AttributeOptionGroup.objects.create(name=u'Цвет')
        attrgroup.save()

    try:
        attrgroupsize = AttributeOptionGroup.objects.get(name='Размер')
    except:
        attrgroupsize = AttributeOptionGroup.objects.create(name=u'Размер')
        attrgroupsize.save()

    try:
        attrcolor = ProductAttribute.objects.get(name=u'Цвет')
    except:
        attrcolor = ProductAttribute.objects.create(
            product_class=product_class,
            name = 'Цвет',
            code = 'color',
            type = 'option',
            option_group = attrgroup
        )
        attrcolor.save()

    try:
        attrsize = ProductAttribute.objects.get(name=u'Размер')
    except:
        attrsize = ProductAttribute.objects.create(
            product_class=product_class,
            name='Размер',
            code='Size',
            type='option',
            option_group=attrgroupsize
        )
        attrcolor.save()

    def parse_post(self,response):
        with open(SCRAPY_DRESS_URL_FILE, 'w+', encoding='utf-8') as f:
            urls = f.read()
        if not response.url in urls:
            upc = response.xpath('//*[@class="i-descr"]/text()').extract()
            title = response.xpath('//*[@class="p-product-name"]/text()').extract()
            price = response.xpath('//*[@class="p-product-price"]/text()').extract()
            description= response.xpath('//*[@class="tab-text-block"]/text()').extract()
            a = response.request.headers.get('Referer', None)
            if not '?' in a:
                category_name = self.cat_by_urls[a[:a.rfind('?')]]
            else:
                category_name = a
            color_name = response.xpath('//*[@class="b-product-params"]/text()').extract()[-1]
            #img = response.xpath('//*[@class="three-september__slide-list three-september__slide-list0 three-september__slide-list--active"]/img/@src').extract()
            #print(img)
            category = create_from_breadcrumbs(category_name)
            category.image = 'categories/cat-1.png'
            category.save()
            product_parent = Product.objects.create(
                structure='parent',
                title=title[0],
                upc=upc[0],
                description=description[0],
                product_class=self.product_class,
            )
            ProductCategory(category=category,product=product_parent).save()
            product_parent.save()

            '''p = requests.get(img[0])
            filename = img[0][img[0].rfind('/')+1::]
            filepath = os.path.join(MEDIA_ROOT,'images/products/'+filename)

            out = open(filepath, "wb")
            out.write(p.content)
            out.close()'''
            ProductImage(product=product_parent,original=os.path.join('images/products/plat-1.png')).save()
            if color_name:
                try:
                    attrcolorname = AttributeOption.objects.get(group=self.attrgroup, option=color_name)
                except:
                    attrcolorname = AttributeOption.objects.create(group=self.attrgroup, option=color_name)
                    attrcolorname.save()

                product_parent.attr.color = attrcolorname
                product_parent.save()

            for size in response.xpath('//*[@class="calc-block p-container__select--container"]/span/text()').extract():
                product_child = Product.objects.create(
                    structure='child',
                    parent=product_parent
                )

                try:
                    sizeattr = AttributeOption.objects.get(group=self.attrgroupsize,option=size)
                except:
                    sizeattr = AttributeOption.objects.create(group=self.attrgroupsize,option=size)
                    sizeattr.save()
                product_child.attr.Size = sizeattr
                product_child.save()
                StockRecord(product=product_child,partner=self.partner, partner_sku=upc[0]+sizeattr.option,price_currency='UAH',price_excl_tax=int(price[0]), num_in_stock=100).save()

        prod = ProductItem()
        prod['url'] = response.url
        return prod



