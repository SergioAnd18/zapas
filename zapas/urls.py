from django.conf.urls import include, url
from django.contrib import admin
from oscar.app import application
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from main.views import home
urlpatterns = [
    # Examples:
    # url(r'^$', 'zapas.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'',include(application.urls)),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    url(r'^static/(?P<path>.*)$','django.views.static.serve',{'document_root': settings.STATIC_ROOT}),
    url(r'^main/', include('main.urls',namespace='main')),
]

#urlpatterns+=staticfiles_urlpatterns()