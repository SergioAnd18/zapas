# coding=utf-8
from django import forms
from .models import ClientMessage, VARIANT_CHOICES, Delivery
from oscar.apps.catalogue.models import ProductAttribute, AttributeOptionGroup, AttributeOption


class MySelect(forms.Select):
    pass


class ClientMessageForm(forms.ModelForm):

    class Meta:
        model = ClientMessage
        fields = '__all__'
        labels = {
            'name':'',
            'email':'',
            'message_text':'',
            'variant':'',
            'phone':''
        }

        widgets = {
            'name': forms.TextInput(attrs={'placeholder':'Ваше имя'}),
            'email': forms.EmailInput(attrs={'placeholder':'E-mail'}),
            'message_text': forms.Textarea(attrs={'placeholder':'Ваш вопрос, сообщение'}),
            'variant': forms.RadioSelect(),
            'phone': forms.TextInput(attrs={'placeholder':'Номер телефона'})
        }

    def clean(self):
        super(forms.ModelForm,self).clean()
        variant = self.cleaned_data.get('variant')
        if variant:
            if int(variant) == 2:
                phone = self.cleaned_data.get('phone')
                if not phone:
                    self.add_error('phone','If choice "call by phone number" you must to put phonenumber!!!')


class FilterForm(forms.Form):
    try:
        gr = AttributeOptionGroup.objects.get(name='Размер')
    except:
        gr = AttributeOptionGroup.objects.create(name = 'Размер')
        gr.save()


    try:
        gr1 = AttributeOptionGroup.objects.get(name='Цвет')
    except:
        gr1 = AttributeOptionGroup.objects.create(name='Цвет')
        gr1.save()

    attrsizelist = AttributeOption.objects.filter(group=gr)
    attrcolorlist = AttributeOption.objects.filter(group=gr1)
    SIZE_CHOICE =  [[i.id,i.option]for i in attrsizelist]
    COLOR_CHOICE = [[i.id,i.option]for i in attrcolorlist]
    AGE_CHOICE = [['1','Для молодых'],['2','Для зрелых'],['3','Для пожелых']]
    BRAND_CHOICE = [['1','Lacoste'],['2','Tommy Hilfiger'],['3','Armani'],['4','Gucci']]
    brand = forms.ChoiceField(choices=BRAND_CHOICE, required=False, label='Выберите бренд')
    age = forms.ChoiceField(choices=AGE_CHOICE,required=False, label='Возрастная категория')
    size = forms.ChoiceField(choices=SIZE_CHOICE,required=False, label='Ваш размер')
    color = forms.ChoiceField(choices=COLOR_CHOICE,required=False, label='Желаемый цвет')
    price_range = forms.CharField(widget=forms.TextInput(attrs={'class':'slider-range'}), label='Цена')


class DeliveryForm(forms.ModelForm):
    class Meta:
        model = Delivery
        fields = ['post','city','sklad','phone_number','pib','email']
        labels = {
            'post':'Оператор доставки',
            'city':'Город доставки',
            'sklad':'Склад доставки',
            'phone_number':'Номер телефона',
            'pib': 'ФИО получателя',
            'email':'E-mail',
        }

