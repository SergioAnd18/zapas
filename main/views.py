# coding=utf-8
from django.shortcuts import render, HttpResponse, get_object_or_404
from main.forms import ClientMessageForm
from django.template import Context
from oscar.apps.basket.models import Line
from main.forms import DeliveryForm
from django.views.decorators.csrf import csrf_protect
from django.template.loader import get_template
from django.core.mail import EmailMessage, send_mail, EmailMultiAlternatives
from zapas.settings import BASE_DIR
import os
#from not oscar.apps.search
# Create your views here.


def home(request):
    return render(request,'home.html',{})


def category_view(request):
    pass


def submitform(request):
    if request.is_ajax():
        form = ClientMessageForm(request.GET)
        if form.is_valid():
            form.save()
            return HttpResponse('ok')
        else:
            return HttpResponse(form.errors.as_json())


def filter(request):
    if request.is_ajax():
        a = request.objects.all
        if 'size' in request.GET:
            request.GET['size']

@csrf_protect
def remove_basket_field(request):
    if request.is_ajax():
        if request.GET:
            line = Line.objects.get(id=int(request.GET['line']))
            line.delete()
            return render(request,'basket/partials/basket_quick.html')


def submt_basket(request):
    if request.is_ajax():
        if request.GET:
            deliveryform = DeliveryForm(request.GET)
            if deliveryform.is_valid():
                delivery = deliveryform.save(commit=False)
                delivery.basket = request.basket
                delivery.save()
                delivery.basket.submit()
                subject  = 'Ваш заказ принят'
                from_email = 'developinc@yandex.ru'
                to = [delivery.email, from_email]
                ctx = {'delivery':delivery}

                message = get_template('email/email.html').render(Context(ctx))

                msg = EmailMultiAlternatives(subject,message,from_email,to)
                msg.attach_alternative(message, "text/html")
                msg.send()
                return HttpResponse('Проверьте почту')
            else:
                return HttpResponse(deliveryform.errors.as_json())
        else:
            print(request.GET)

def refresh_basketline(request):
    if request.is_ajax():
        if request.GET:
            line = get_object_or_404(Line,id=int(request.GET['line']))
            line.quantity = int(request.GET['quantity'])
            line.save()
            return render(request,'basket/partials/basket_quick.html',{})


