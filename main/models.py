# coding=utf-8
# This Python file uses the following encoding: utf-8
from django.db import models
from oscar.apps.basket.models import Basket

VARIANT_CHOICES = [
    ['1', u'ответить на почту'],
    ['2', u'перезвонить на мобильный'],
]


class ClientMessage(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField()
    message_text = models.TextField()
    variant = models.CharField(choices=VARIANT_CHOICES, max_length=100)
    phone = models.CharField(blank=True, max_length=16)


class City(models.Model):
    city_name = models.CharField(max_length=50)
    operator = models.CharField(max_length=50)

    def __unicode__(self):
        return self.city_name


class Post(models.Model):
    post_name = models.CharField(max_length=30)

    def __unicode__(self):
        return self.post_name


class Delivery(models.Model):
    post = models.ForeignKey(Post)
    city = models.ForeignKey(City)
    sklad = models.CharField(max_length=20)
    phone_number = models.CharField(max_length=10)
    pib = models.CharField(max_length=100)
    email = models.EmailField()
    basket = models.OneToOneField(Basket)

    def __unicode__(self):
        return '%s %s' %(self.pib, self.basket)