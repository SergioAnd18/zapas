from django import template
from main.forms import ClientMessageForm, FilterForm, DeliveryForm

register = template.Library()


@register.inclusion_tag('partials/clientmessage.html')
def showform():
    form=ClientMessageForm()
    return {'clientform':form}


@register.inclusion_tag('partials/filterform.html')
def filterform():
    form=FilterForm()
    return {'filterform':form}


@register.inclusion_tag('partials/delivery.html')
def deliveryform():
    return  {'deliveryform':DeliveryForm().as_p()   }