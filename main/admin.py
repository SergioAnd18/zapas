from django.contrib import admin
from main.models import ClientMessage, Post, City, Delivery
# Register your models here.

admin.site.register(ClientMessage)
admin.site.register(Post)
admin.site.register(City)
admin.site.register(Delivery)
