from main.views import *
from django.conf.urls import url
from main.views import submitform, remove_basket_field, submt_basket, refresh_basketline

urlpatterns = [
    url(r'^submitmsg/$', submitform, name='submitmsg'),
    url(r'^removeline/$', remove_basket_field, name='removeline'),
    url(r'^submit_basket/$', submt_basket, name='submt_basket'),
    url(r'^update_basket/$', refresh_basketline, name='update_basket'),
]