# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('basket', '0006_auto_20160111_1108'),
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('city_name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Delivery',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('operator', models.CharField(max_length=50)),
                ('basket', models.OneToOneField(to='basket.Basket')),
            ],
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='PostSklad',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sklad', models.CharField(max_length=50)),
                ('city', models.ForeignKey(to='main.City')),
                ('post', models.ForeignKey(to='main.Post')),
            ],
        ),
        migrations.AlterField(
            model_name='clientmessage',
            name='variant',
            field=models.CharField(max_length=100, choices=[[b'1', '\u043e\u0442\u0432\u0435\u0442\u0438\u0442\u044c \u043d\u0430 \u043f\u043e\u0447\u0442\u0443'], [b'2', '\u043f\u0435\u0440\u0435\u0437\u0432\u043e\u043d\u0438\u0442\u044c \u043d\u0430 \u043c\u043e\u0431\u0438\u043b\u044c\u043d\u044b\u0439']]),
        ),
        migrations.AddField(
            model_name='delivery',
            name='sklad',
            field=models.ForeignKey(to='main.PostSklad'),
        ),
    ]
