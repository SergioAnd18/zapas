# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ClientMessage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('email', models.EmailField(max_length=254)),
                ('message_text', models.TextField()),
                ('variant', models.CharField(max_length=100, choices=[(b'1', '\u043e\u0442\u0432\u0435\u0442\u0438\u0442\u044c \u043d\u0430 \u043f\u043e\u0447\u0442\u0443'), (b'2', '\u043f\u0435\u0440\u0435\u0437\u0432\u043e\u043d\u0438\u0442\u044c \u043d\u0430 \u043c\u043e\u0431\u0438\u043b\u044c\u043d\u044b\u0439')])),
                ('phone', models.CharField(max_length=16, blank=True)),
            ],
        ),
    ]
