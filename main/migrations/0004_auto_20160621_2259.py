# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_auto_20160620_2209'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='postsklad',
            name='city',
        ),
        migrations.RemoveField(
            model_name='postsklad',
            name='post',
        ),
        migrations.RemoveField(
            model_name='post',
            name='name',
        ),
        migrations.AddField(
            model_name='delivery',
            name='city',
            field=models.ForeignKey(default=1, to='main.City'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='delivery',
            name='post',
            field=models.ForeignKey(default=1, to='main.Post'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='post',
            name='post_name',
            field=models.CharField(default=1, max_length=30),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='delivery',
            name='sklad',
            field=models.CharField(max_length=20),
        ),
        migrations.DeleteModel(
            name='PostSklad',
        ),
    ]
