# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_auto_20160620_2109'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='delivery',
            name='operator',
        ),
        migrations.AddField(
            model_name='city',
            name='operator',
            field=models.CharField(default=1, max_length=50),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='delivery',
            name='phone_number',
            field=models.CharField(default=1, max_length=10),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='delivery',
            name='pib',
            field=models.CharField(default=1, max_length=100),
            preserve_default=False,
        ),
    ]
