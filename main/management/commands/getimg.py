from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
import os
from oscar.apps.catalogue.models import Product, ProductImage
from zapas.settings import BASE_DIR


class Command(BaseCommand):
    help='Add image to Product from folder'

    def handle(self, *args, **options):
        dir_path = os.path.join(BASE_DIR,'media/images/products')
        path_for_file = 'images/products'
        all_files = [i for i in os.listdir(dir_path) if(os.path.isfile(os.path.join(dir_path,i)))and('_' in i)]
        self.stdout.write(str(all_files))
        for image in all_files:
            upc, priority = image[:image.rfind('.')].split('_')
            self.stdout.write(upc)
            try:
                product=Product.objects.get(upc=upc)
            except:
                continue
            original=os.path.join(path_for_file, image)

            try:
                ProductImage.objects.get(original=original)
                self.stdout.write('image %s was added' % image)
            except:
                ProductImage(
                    product=product,
                    original=original,
                    display_order=int(priority)-1
                ).save()
                self.stdout.write('image %s added' % image)
        self.stdout.write('all images successfully added')