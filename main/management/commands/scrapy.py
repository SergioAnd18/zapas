from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
import os
os.chdir(settings.CRAWLER_PATH)


class Command(BaseCommand):

    def run_from_argv(self, argv):
        self._argv = argv
        self.execute()

    def handle(self, *args, **options):
        from scrapy.cmdline import execute
        execute(self._argv[1:])